## Solution for frontend task 1 

This repo contains a plain javascript webapp, designed to the spec from pdf given with the task.

The repo is only contains one html page. The javascript and css used in this project is placed in the corresponding folders. In the css folder you will se that i used scss and in the javascript folder you will find that it is split into 4 different parts, these are described below:
* `index.js`, which contains the state of the application and manages the Rendering class and UiInteraction class
* `UiInteraction.js`, which is responsible for modifying the state of the app as the user interacts with the UI.
* `Renderer.js`, which only reads the state of the app and renders it in the html
* `Models folder`, which contains the data models used in the app, as well as defining som mock data