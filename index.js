import { testLaptops } from "./javascript/models/laptop.js";
import { Renderer } from "./javascript/Renderer.js"
import { UiInteraction } from "./javascript/UiInteraction.js"

class App {
    constructor() {
        this.state = {
            hasLoan: false,
            laptops: testLaptops,
            selectedLaptop: testLaptops[0],
            balance: 0,
            debt: 0,
            pay: 0,
            time: 10,
            ownedLaptops: [],
            stats: {
                totalMoneyBorrowed: 0,
                totalDebtRepaid: 0,
                totalInterestsOnDebt: 0,
                totalWorkHours: 0,
                totalSpentOnLaptops: 0,
            }

        };
        this.renderer = new Renderer(this.state);
        this.uiInteraction = new UiInteraction(this.renderer, this.state);
    }

    init() {
        this.renderer.initLaptopSelection();
        this.renderer.render();
        this.uiInteraction.setButtonFunctions();
        this.uiInteraction.startTimer();
    }
}

new App().init();