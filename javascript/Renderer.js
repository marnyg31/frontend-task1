export class Renderer {
    constructor(state) {
        this.state = state;

        this.elLaptopName = document.getElementById("lapName");
        this.elLaptopPrice = document.getElementById("lapPrice");
        this.elLaptopDescripton = document.getElementById("lapDesc");
        this.elLaptopImage = document.getElementById("lapImg");
        this.elFeatureList = document.getElementById("FeatureList");
        this.elLaptopSelection = document.getElementById("laptopSelection");
        this.elPay = document.getElementById("pay");
        this.elBalance = document.getElementById("balance");
        this.elTimer = document.getElementById("timer");
        this.elDebt = document.getElementById("debt");
        this.elStatsList = document.getElementById("statsList");
        this.elOwnedLaptops = document.getElementById("ownedLaptops");
    }

    setPageInfo() {
        this.elLaptopName.textContent = this.state.selectedLaptop.name;
        this.elLaptopPrice.textContent = this.state.selectedLaptop.price;
        this.elLaptopDescripton.textContent = this.state.selectedLaptop.description;
        this.elLaptopImage.src = this.state.selectedLaptop.image;
    }

    setStatsInfo() {
        this.elStatsList.textContent = ""
        const elMoneyBorrowed = document.createElement("li")
        elMoneyBorrowed.textContent = "Total money borrowed: " + this.state.stats.totalMoneyBorrowed + " kr"
        const elDebtRepaid = document.createElement("li")
        elDebtRepaid.textContent = "Total Debt repaid: " + this.state.stats.totalDebtRepaid + " kr"
        const elInterestGained = document.createElement("li")
        elInterestGained.textContent = "Total interest accrued: " + Number(this.state.stats.totalInterestsOnDebt).toFixed(2) + " kr"
        const elTotalHoursWorked = document.createElement("li")
        elTotalHoursWorked.textContent = "Total hours worked: " + this.state.stats.totalWorkHours + " hours"
        const elMoneyEarned = document.createElement("li")
        elMoneyEarned.textContent = "Total money earned: " + this.state.stats.totalWorkHours * 100 + " kr"
        const elMoneySpentOnLaptops = document.createElement("li")
        elMoneySpentOnLaptops.textContent = "Total spent on laptops: " + this.state.stats.totalSpentOnLaptops + " kr"

        this.elStatsList.appendChild(elMoneyBorrowed)
        this.elStatsList.appendChild(elDebtRepaid)
        this.elStatsList.appendChild(elInterestGained)
        this.elStatsList.appendChild(elTotalHoursWorked)
        this.elStatsList.appendChild(elMoneyEarned)
        this.elStatsList.appendChild(elMoneySpentOnLaptops)

    }

    setFeatures() {
        const elFeature = this.state.selectedLaptop.features.map((f) => {
            let el = document.createElement("li");
            el.textContent = f;
            return el;
        });

        this.elFeatureList.textContent = "";
        elFeature.forEach((el) => this.elFeatureList.appendChild(el));
    }

    setOwnedLaptops() {
        this.elOwnedLaptops.textContent = ""
        let ownedLaptops = ["nothing", "empty", "you own nothing"];

        if (this.state.ownedLaptops.length > 0)
            ownedLaptops = this.state.ownedLaptops.map(l => l.name);

        ownedLaptops.forEach(el => {
            let listEl = document.createElement("li");
            listEl.textContent = el;
            this.elOwnedLaptops.appendChild(listEl);
        });
    }

    setMoneyInfo() {
        this.elPay.textContent = this.state.pay;
        this.elBalance.textContent = this.state.balance;
        this.elTimer.textContent = this.state.time;
        this.elDebt.textContent = Number(this.state.debt).toFixed(2);
    }

    initLaptopSelection() {
        this.state.laptops.forEach((l) => {
            const option = document.createElement("option");
            option.value = l.id;
            option.text = l.name;
            option.onselect = () => console.log(this);
            this.elLaptopSelection.appendChild(option);
        });
    }
    render() {
        this.setPageInfo();
        this.setFeatures();
        this.setMoneyInfo();
        this.setOwnedLaptops();
        this.setStatsInfo();

    }
}