export class UiInteraction {
    constructor(renderer, state) {
        this.renderer = renderer;
        this.state = state;
    }
    setButtonFunctions() {
        document.getElementById("buyButton").addEventListener("click", () => this.buyButtonFunction());
        document.getElementById("bankButton").addEventListener("click", () => this.bankButtonFunction());
        document.getElementById("workButton").addEventListener("click", () => this.workButtonFunction());
        document.getElementById("loanButton").addEventListener("click", () => this.loanButtonFunction());
        document.getElementById("repayButton").addEventListener("click", () => this.repayButtonFunction());
        document.getElementById("laptopSelection").addEventListener("change", (e) => this.changeLaptopSelection(e));

        document.getElementById("buyButton").addEventListener("click", () => this.renderer.render());
        document.getElementById("bankButton").addEventListener("click", () => this.renderer.render());
        document.getElementById("workButton").addEventListener("click", () => this.renderer.render());
        document.getElementById("loanButton").addEventListener("click", () => this.renderer.render());
        document.getElementById("laptopSelection").addEventListener("change", () => this.renderer.render());
    }

    startTimer() {
        setInterval(() => {
            if (this.state.time <= 0) {
                this.state.debt += this.state.debt * 0.1
                this.state.stats.totalInterestsOnDebt += (this.state.debt * 0.1)
                this.state.time = 10;
            } else this.state.time--;
            this.renderer.render()
        }, 1000);

    }

    repayButtonFunction() {
        if (this.state.debt < this.state.balance) {
            this.state.balance -= this.state.debt
            this.state.stats.totalDebtRepaid += this.state.debt
            this.state.debt = 0
        } else {
            this.state.debt -= this.state.balance
            this.state.stats.totalDebtRepaid += this.state.balance
            this.state.balance = 0
        }

    }

    changeLaptopSelection(e) {
        this.state.selectedLaptop = this.state.laptops.find(laptop => laptop.id == e.target.value);
    };

    workButtonFunction() {
        this.state.pay += 100;
        this.state.stats.totalWorkHours++;
    }

    buyButtonFunction() {
        if (this.state.balance >= this.state.selectedLaptop.price) {
            this.state.balance = this.state.balance - this.state.selectedLaptop.price;
            this.state.hasLoan = false;
            this.state.stats.totalSpentOnLaptops += this.state.selectedLaptop.price;
            this.state.ownedLaptops.push(this.state.selectedLaptop);
        } else {
            alert("You are to poor... ");
            alert("lol, u suck");
        }
    }

    bankButtonFunction() {
        this.state.balance += this.state.pay;
        this.state.pay = 0;
    }


    loanButtonFunction() {
        if (this.userHasLoan()) return;
        const { loanSize, loan } = this.readUserInput();
        if (loanSize == null) return;
        this.state.hasLoan = true;
        this.state.balance += loan;
        this.state.debt += loan;
        this.state.stats.totalMoneyBorrowed += loan;
    }

    userHasLoan() {
        if (!this.state.hasLoan) return false;
        alert("You already have a loan\nBuy new pc to allow for more free mony");
        return true;
    }

    readUserInput() {
        do {
            var loanSize = prompt("Please enter loan size, make it no larger than " +
                this.state.balance * 2, "100");
            var loan = parseInt(loanSize);
        } while (loan == NaN || loan > 2 * this.state.balance);
        return { loanSize, loan };
    }

}