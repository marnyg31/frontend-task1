export class Laptop {
    constructor(id, name, price, description, features, image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.description = description;
        this.features = features;
        this.image = image;
    }
}

export const testLaptops = [
    new Laptop(
        1,
        "Default Andy",
        1000,
        "Sunt ex pariatur cupidatat dolor. Tempor consequat pariatur Lorem eiusmod sunt ut pariatur minim nulla nulla nostrud do. Elit eu dolor aliquip do deserunt exercitation officia nulla dolore ex eu.", ["fast", "stronk", "smol", "sxy"],
        "https://cdn.mos.cms.futurecdn.net/6t8Zh249QiFmVnkQdCCtHK-320-80.jpg"
    ),
    new Laptop(
        2,
        "BIIG booooi",
        2000,
        "Veniam nulla magna id incididunt ut. Ipsum sunt deserunt dolore cupidatat velit culpa consequat duis velit laboris. Adipisicing consectetur irure elit ea in.", ["fast", "stronk", "sxy"],
        "https://img-prod-cms-rt-microsoft-com.akamaized.net/cms/api/am/imageFileData/RE3oYjc?ver=a28c"
    ),
    new Laptop(
        3,
        "The future",
        420,
        "Velit culpa deserunt officia Lorem officia duis. Qui id cillum dolore consectetur in ullamco laboris. Id sint culpa aliqua ut eu dolor eiusmod cillum proident nostrud esse ad adipisicing. Ea ut Lorem velit non nostrud deserunt do.", ["fast", "smol", "sxy"],
        "https://images7.memedroid.com/images/UPLOADED645/5c53290f46ed4.jpeg"
    ),
    new Laptop(
        4,
        "Avrage Joe",
        666,
        "Officia commodo nisi ut pariatur labore voluptate consequat mollit dolore nisi ut. Pariatur magna incididunt nostrud labore. Culpa nisi ut quis qui exercitation ex nulla cillum.", ["fast", "stronk", "biig", "sxy"],
        "https://media.power-cdn.net/images/h-fdc0424515ecd921629fd1cb7240a144/products/1013109/1013109_5_600x600_w_g.jpg"
    ),
    new Laptop(
        5,
        "Apple 1",
        50000,
        "it apple, what more do you need. Stand sold separately, for only 1000$ 😲", ["fast", "smol", "appl"],


        "https://img.memecdn.com/APPLE-LAPTOP_o_123623.jpg"
    ),
];